package shared.artefact.pattern.ot;

import org.waveprotocol.wave.model.document.operation.BufferedDocOp;

public class Operation {
	
	private BufferedDocOp mutationComponents;
	private String versionNumber;
	private String parentage;
	
	
	public Operation(BufferedDocOp mutationComponents, String versionNumber, String parentage) {
		super();
		this.mutationComponents = mutationComponents;
		this.versionNumber = versionNumber;
		this.parentage = parentage;
	}

	public BufferedDocOp getMutationComponents() {
		return mutationComponents;
	}

	public void setMutationComponents(BufferedDocOp mutationComponents) {
		this.mutationComponents = mutationComponents;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getParentage() {
		return parentage;
	}

	public void setParentage(String parentage) {
		this.parentage = parentage;
	}

	@Override
	public String toString() {
		return "Operation [mutationComponents=" + mutationComponents
				+ ", versionNumber=" + versionNumber + ", parentage="
				+ parentage + "]";
	}
}