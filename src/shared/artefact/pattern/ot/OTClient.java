package shared.artefact.pattern.ot;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.camel.impl.SimpleRegistry;
import org.waveprotocol.wave.model.document.operation.BufferedDocOp;
import org.waveprotocol.wave.model.document.operation.impl.BufferedDocOpImpl.DocOpBuilder;
import org.waveprotocol.wave.model.operation.OperationException;

import shared.artefact.pattern.simulation.IClient;


public class OTClient implements IClient {
	private String id;
	private String oldText;
	private SimpleRegistry registry;
	private OTServer  objectServer;
	private String parentage;
	private int versionNumber;
	private OperationHandler opHandler;
	private boolean acknowledge;
	private String lastSentOp;
	private int changes;
	private int sentDataSize;

	public OTClient(String id, SimpleRegistry registry, String objectId) {
		this.id = id;
		this.registry = registry;
		objectServer = registry.lookup(objectId, OTServer.class);
		versionNumber = 0;
		parentage = null;
		oldText = objectServer.register(this);
		opHandler = new OperationHandler(oldText);
		acknowledge = true;
		lastSentOp = null;
		changes = 0;
		sentDataSize = 0;
	}

	@Override
	public void update(String update, int pos){
	
		Operation op = generateOperation(update, pos);
		parentage = op.getVersionNumber();
		oldText = update;
		
		if(acknowledge) {
			
			lastSentOp = op.getVersionNumber();
			acknowledge = false;
			
			changes++;
			sentDataSize += calculateSentData(op.getMutationComponents());
			
			objectServer.receiveOp(op);
						
				try {
					opHandler.update(op);
					
				} catch (OperationException e) {
					e.printStackTrace();
				}
			
		} else {
			
			try {
				opHandler.buffer(op);
			} catch (OperationException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void receiveServerRequests(Operation op){
		
		sentDataSize += calculateSentData(op.getMutationComponents());
		
		if(op.getVersionNumber().equals(lastSentOp)){
			
			Operation buffer = opHandler.flushBuffer();
			
			if(buffer == null) {
				
				acknowledge = true;
				
			} else {
				sentDataSize += calculateSentData(buffer.getMutationComponents());
				objectServer.receiveOp(buffer);
				lastSentOp = buffer.getVersionNumber();
			}
			
		} else {
			
			try {
				
				opHandler.applyOp(op);
				oldText = opHandler.getCurrentClientState().getCharactersString(0);
				parentage = op.getVersionNumber();
				changes++;
				
			} catch (OperationException e) {
				e.printStackTrace();
			}
		}
	}
	
	private int calculateSentData(BufferedDocOp op){
		
		int result = 0;
		String clean = "";
		
		Pattern p = Pattern.compile("\"([^\"]*)\"");
		
		Matcher m = p.matcher(op.toString());
		while(m.find()){		
			clean += m.group(1);
		}
		
		p = Pattern.compile("__(.*?);");
		m = p.matcher(op.toString());
		
		while(m.find()){		
			clean += m.group(1);
		}
		
		boolean increase = true;
		try{
			op.getRetainItemCount(op.size()-1);
		
		}catch(IllegalArgumentException e){
				
			increase = false;
		}
		
		if(increase){
			result++;
		}
		
		result += clean.length();
		
		return result;
	}

	
	private Operation generateOperation(String update, int pos) {
		
		DocOpBuilder builder = new DocOpBuilder();
	
		if(pos != 0) {
				builder.retain(pos);
		}
		
		String delete = oldText.substring(pos, pos+update.length());
		builder.deleteCharacters(delete);
		builder.characters(update);
		builder.retain(oldText.length()-pos-update.length());
		versionNumber++;
	
		return new Operation(builder.finish(), id+":"+versionNumber, parentage);
	}

	@Override
	public String get() {
		
		return opHandler.getCurrentClientState().getCharactersString(0);
	}

	@Override
	public String getId(){
		return id;
	}
	
	@Override
	public int getChanges() {
		
		return changes;
	}

	@Override
	public int getsentDataSize() {
		
		return sentDataSize;
	}
}
