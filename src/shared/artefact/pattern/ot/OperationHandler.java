package shared.artefact.pattern.ot;

import org.waveprotocol.wave.model.document.operation.BufferedDocOp;
import org.waveprotocol.wave.model.document.operation.algorithm.Composer;
import org.waveprotocol.wave.model.document.operation.algorithm.Transformer;
import org.waveprotocol.wave.model.document.operation.impl.BufferedDocOpImpl.DocOpBuilder;
import org.waveprotocol.wave.model.operation.OperationException;
import org.waveprotocol.wave.model.operation.OperationPair;

public class OperationHandler {
	
	private Operation bufferOp;
	private BufferedDocOp appliedOps;
	private BufferedDocOp bridgeLink;
	private BufferedDocOp buffer;
	
	
	public OperationHandler(String text) {
		
		if(text == null || text == ""){
			appliedOps = new DocOpBuilder().finish();
		} else {
			DocOpBuilder builder = new DocOpBuilder();
			builder.characters(text);
			appliedOps = builder.finish();
		}
		
		bridgeLink = new DocOpBuilder().finish();
		buffer = new DocOpBuilder().finish();
		bufferOp = null;
	}

	public void update(Operation op) throws OperationException{
		
		appliedOps = Composer.compose(appliedOps, op.getMutationComponents());
		bridgeLink = op.getMutationComponents();
		
	}

	public void buffer(Operation op) throws OperationException{
		
		buffer = Composer.compose(buffer, op.getMutationComponents());
		appliedOps = Composer.compose(appliedOps, op.getMutationComponents());
		
		if(bufferOp == null) {
			
			bufferOp = op;
		}
	}
	
	public void applyOp(Operation op) throws OperationException{
		
		OperationPair<BufferedDocOp> transPairBridge = Transformer.transform(bridgeLink, op.getMutationComponents());
		
		if(bufferOp != null){
			bufferOp.setParentage(op.getVersionNumber());
			OperationPair<BufferedDocOp> transPairBuffer = Transformer.transform(buffer, transPairBridge.serverOp());
			appliedOps = Composer.compose(appliedOps, transPairBuffer.serverOp());
			buffer = transPairBuffer.clientOp();
			bridgeLink = transPairBridge.clientOp();
			
		} else {
			appliedOps = Composer.compose(appliedOps, op.getMutationComponents());
		}
	}
	
	public Operation flushBuffer(){
		
		Operation result = null;
		
		if(bufferOp != null) {
			
			result = bufferOp;
			bufferOp = null;
			bridgeLink = buffer;
			result.setMutationComponents(buffer);
			buffer = new DocOpBuilder().finish();	
			
		}
		
		return result;
	}
	
	public BufferedDocOp getCurrentClientState(){
		return appliedOps;
	}
}
