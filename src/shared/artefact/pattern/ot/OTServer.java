package shared.artefact.pattern.ot;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.waveprotocol.wave.model.document.operation.BufferedDocOp;
import org.waveprotocol.wave.model.document.operation.algorithm.Composer;
import org.waveprotocol.wave.model.document.operation.algorithm.Transformer;
import org.waveprotocol.wave.model.document.operation.impl.BufferedDocOpImpl.DocOpBuilder;
import org.waveprotocol.wave.model.operation.OperationException;
import org.waveprotocol.wave.model.operation.OperationPair;
public class OTServer{

	private String id; 
	private HashMap<String, Operation> operations;
	private ArrayDeque<String> history;
	private ArrayList<OTClient> clients;
	private BufferedDocOp appliedOps;
	int count = 0;

	public OTServer(String id, String text) {
		this.id = id;
		operations = new HashMap<String, Operation>();
		history = new ArrayDeque<String>();
		clients = new ArrayList<OTClient>();
		DocOpBuilder builder = new DocOpBuilder();
		builder.characters(text);
		appliedOps = builder.finish();
	}

	public String register(OTClient client) {

		synchronized (clients) {

			for (OTClient clientLocal : clients) {

				if (clientLocal.getId().equals(client.getId())) {
					return null;
				}
			}
			clients.add(client);
		}
		return getDoc();
	}

	private synchronized void broadcast(Operation op) {

		for (OTClient client : clients) {
			
			client.receiveServerRequests(op);

		}
	}

	public synchronized void receiveOp(Operation op) {
		
		Iterable<BufferedDocOp> historyOperations = null;
		
		if (op.getParentage() == null || operations.containsKey(op.getParentage())) {

			if (op.getParentage() != null) {
				historyOperations = getHistory(op.getParentage());
			}

		} else {
			
			System.out.println("ERROR: Operation not in server state space");
		}

		BufferedDocOp docOp = null;
	
		if (historyOperations != null) {
			
			docOp = Composer.compose(historyOperations);
		}
		
		Transformer trans = new Transformer();
		OperationPair<BufferedDocOp> transPair;
		if (docOp == null) {
			
			try {
				
				appliedOps = Composer.compose(appliedOps, op.getMutationComponents());
				broadcast(op);
				history.push(op.getVersionNumber());
				operations.put(op.getVersionNumber(), op);
			} catch (OperationException e) {
	
				e.printStackTrace();
			}

		} else {

			try {
				transPair = trans.transformOperations(op.getMutationComponents(), docOp);
				Operation newOp = new Operation(transPair.clientOp(), op.getVersionNumber(), op.getParentage());

				appliedOps = Composer.compose(appliedOps, newOp.getMutationComponents());

				broadcast(op);
				history.push(newOp.getVersionNumber());
				operations.put(newOp.getVersionNumber(), newOp);
			} catch (OperationException e) {

				e.printStackTrace();
			}
		}
		count++;
	}
	
	private String getDoc(){
		
		return appliedOps.getCharactersString(0);
	}

	private synchronized List<BufferedDocOp> getHistory(String parentage) {

		ArrayList<BufferedDocOp> result = new ArrayList<BufferedDocOp>();
		ArrayDeque<String> historyClone;
		
		synchronized (history) {
			historyClone = history.clone();
		}
		
		Iterator<String> iter = historyClone.iterator();
		
		String opId = iter.next();

		while (!opId.equals(parentage) && iter.hasNext()) {
			result.add(0, operations.get(opId).getMutationComponents());
			opId = iter.next();
		}

		return result;
	}

	public String getId() {
		
		return id;
	}
}
