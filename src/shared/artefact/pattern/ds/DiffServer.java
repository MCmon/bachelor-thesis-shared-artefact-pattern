package shared.artefact.pattern.ds;

import java.util.LinkedList;
import java.util.Set;

import org.apache.camel.impl.SimpleRegistry;

import shared.artefact.pattern.ds.diff_match_patch.Diff;



public class DiffServer{

	private String id;
	private String serverText;
	private SimpleRegistry registryClient;
	private SimpleRegistry registryServerShadows;
	private diff_match_patch syncHandler;

	public DiffServer(String id, SimpleRegistry registryClient, String text) {
		this.id = id;
		this.registryClient = registryClient;
		registryServerShadows = new SimpleRegistry();
		syncHandler = new diff_match_patch();
		serverText = text;
	}

	public synchronized String registerClient(String clientId) {

		String result = "";
		if (!registryClient.containsKey(clientId)) {
			registryServerShadows.put(clientId, serverText);
			result = serverText;
		}

		return result;
	}

	public synchronized void update(LinkedList<Diff> diffs, String clientId) {

		String serverShadow = registryServerShadows.lookup(clientId,
				String.class);


		// ########### sync part ###################

		serverShadow = syncHandler.patch_apply(syncHandler.patch_make(diffs),
				serverShadow)[0].toString();

		registryServerShadows.put(clientId, serverShadow);

		serverText = syncHandler.patch_apply(syncHandler.patch_make(diffs),
				serverText)[0].toString();

		Set<String> clientIdSet = registryServerShadows.keySet();

		LinkedList<Diff> newDiffs;

		for (String id : clientIdSet) {

			if (!id.equals(clientId)) {

				serverShadow = registryServerShadows.lookup(id, String.class);

				newDiffs = syncHandler.diff_main(serverShadow, serverText);
				syncHandler.diff_cleanupSemantic(newDiffs);

				DiffClient client = getClient(id);
				client.update(newDiffs);

				registryServerShadows.put(id, serverText);
			}
		}
	}

	private synchronized DiffClient getClient(String id) {

		return registryClient.lookup(id, DiffClient.class);
	}
}
