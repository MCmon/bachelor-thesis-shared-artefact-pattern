package shared.artefact.pattern.ds;

import java.util.LinkedList;

import org.waveprotocol.wave.model.document.operation.BufferedDocOp;
import org.waveprotocol.wave.model.document.operation.algorithm.Composer;
import org.waveprotocol.wave.model.document.operation.algorithm.Transformer;
import org.waveprotocol.wave.model.document.operation.impl.BufferedDocOpImpl;
import org.waveprotocol.wave.model.document.operation.impl.BufferedDocOpImpl.DocOpBuilder;
import org.waveprotocol.wave.model.id.WaveId;
import org.waveprotocol.wave.model.id.WaveletId;
import org.waveprotocol.wave.model.operation.OperationException;
import org.waveprotocol.wave.model.operation.OperationPair;
import org.waveprotocol.wave.model.wave.data.impl.WaveletDataImpl;

import shared.artefact.pattern.ds.diff_match_patch.*;

public class Test {

	public static void main(String[] args) throws OperationException {
		diff_match_patch syncHandler = new diff_match_patch();

		String text1 = "aaal";
		String text2= "bbb";
		String text = "cat";
		
		LinkedList<Diff> diffs = syncHandler.diff_main(text1, text2);
		syncHandler.diff_cleanupSemantic(diffs);
		
		for(Diff diff : diffs){
			System.out.println(diff.text);
		}
		
		System.out.println(diffs);
		
		
		
		/*DocOp m;
		BufferedDocOp op = new DocOpUtil().buffer(m);
	
		;
		Transformer trans = new Transformer();
		trans.transformOperations(arg0, arg1);
		*/
		/*DocOpBuilder build= new DocOpBuilder();
		
		
		BufferedDocOpImpl e = (BufferedDocOpImpl) build.finish();
		
		build.characters("go");

		
		e = (BufferedDocOpImpl) build.finish();
		BufferedDocOp doc;
		String es = e.toString();
		System.out.println("#operation "+e.size()+" op.size: "+e.toString());	
		//BootstrapDocument doc = new BootstrapDocument();
		WaveletDataImpl wavlet = new WaveletDataImpl(new WaveId("d", "a"), new WaveletId("d", "b"));
		wavlet.modifyDocument("test1", e);
		wavlet.modifyDocument("test2", e);
		
		
		
		
		
		
		System.out.println(wavlet);
		
		// ############## Ab hier wird bsp aus code commit nachgebaut!!!!!!!
		
		
		DocOpBuilder build1= new DocOpBuilder();
		DocOpBuilder build2= new DocOpBuilder();
		build1.retain(2);
		build1.characters("a");
		
		BufferedDocOpImpl change1 = (BufferedDocOpImpl) build1.finish();
		
		wavlet.modifyDocument("test1", change1);
		System.out.println(wavlet.getDocuments());
		build2.retain(2);
		build2.characters("t");
		
		BufferedDocOpImpl change2 = (BufferedDocOpImpl) build2.finish();
		
		wavlet.modifyDocument("test2", change2);
		System.out.println(wavlet.getDocuments());
		
		Transformer trans = new Transformer();
		OperationPair<BufferedDocOp> transPair =  trans.transformOperations(change1, change2);
		System.out.println(transPair.clientOp());
		wavlet.modifyDocument("test1", transPair.serverOp());
		wavlet.modifyDocument("test2", transPair.clientOp());
		
		System.out.println(wavlet.getDocuments());
		
		DocOpBuilder b1= new DocOpBuilder();
		DocOpBuilder b2= new DocOpBuilder();
		DocOpBuilder b3= new DocOpBuilder();
		String s = "a1011001a001101aaa0000001aaaa10100011aaa";
		System.out.println(s.length());
		b1.characters(s);
		b2.retain(1);
		b2.deleteCharacters("aaaaaaa");
		b2.characters("1011001");
		b2.retain(1);
		b2.deleteCharacters("aaaaa");
		b2.characters("0");
		b2.deleteCharacters("a");
		b2.characters("01101");
		b2.retain(3);
		b2.deleteCharacters("aaaaaaa");
		b2.characters("0000001");
		b2.retain(4);
		b2.deleteCharacters("aaaaa");
		b2.characters("10100");
		b2.retain(6);
		
		b3.retain(5);
		b3.deleteCharacters("001a0");
		b3.characters("10001");
		b3.retain(30);
		
		Transformer transf = new Transformer();
		OperationPair<BufferedDocOp> transPairnew;
		
		BufferedDocOp test = b3.finish();
		
		System.out.println("SUMM: "+test.getRetainItemCount(0)+", "+test.getDeleteCharactersString(1).length()+", "+test.getCharactersString(2)+", "+test.getRetainItemCount(3));
		
		transPairnew = transf.transform(b3.finish(), b2.finish());
		System.out.println("clientOp: "+transPairnew.clientOp()+"\n serverOp: "+transPairnew.serverOp());
		
		String result = Composer.compose(b1.finish(),transPairnew.clientOp()).getCharactersString(0);
		System.out.println(result);
		System.out.println(result.length());
		
		BufferedDocOp buffer = new DocOpBuilder().finish();
		
		buffer = Composer.compose(buffer, test);
		
		/*DocOpBuilder build1= new DocOpBuilder();
		DocOpBuilder build2= new DocOpBuilder();
		
		//build1.characters("got");
		build2.retain(2);
		build2.characters("a");
		build2.retain(1);
		
		System.out.println(Composer.compose(build1.finish(), build2.finish()));*/
		
		
	}

}
