package shared.artefact.pattern.ds;

import java.util.LinkedList;
import java.util.List;

import org.apache.camel.impl.SimpleRegistry;

import shared.artefact.pattern.ds.diff_match_patch.Diff;
import shared.artefact.pattern.simulation.IClient;

public class DiffClient implements IClient{
	
	private String id;
	private String clientText;
	private String clientShadow;
	private SimpleRegistry registry;
	private DiffServer  objectServer;
	private diff_match_patch syncHandler;
	private int changes;
	private int sentDataSize;

	public DiffClient(String id, SimpleRegistry registry, String objectServerId) {
		this.id = id;
		this.registry = registry;
		objectServer = registry.lookup(objectServerId, DiffServer.class);
		clientText = objectServer.registerClient(id);
		clientShadow = clientText;
		syncHandler = new diff_match_patch();
		changes = 0;
		sentDataSize = 0;
	}

	@Override
	public void update(String update, int pos) {
		
		changes++;
		
		clientText = clientShadow.substring(0, pos)+update+clientShadow.substring(pos+update.length());;
		
		LinkedList<Diff> diffs = syncHandler.diff_main(clientShadow, clientText);
		syncHandler.diff_cleanupSemantic(diffs);
		sentDataSize += calcSentData(diffs);
		clientShadow = clientText;
		objectServer.update(diffs, id);
	}
	
	private int calcSentData(List<Diff> diffs){
		
		int result = 0;
		
		for(Diff diff : diffs){	
			result += diff.text.length();
		}
		
		return result;
	}
	
	/**
	 * This method makes the differential synchronization client side
	 * @param diffs is a list of changes which have been made to the servertext
	 */
	public void update(LinkedList<Diff> diffs){
		
		sentDataSize += calcSentData(diffs);
	
			clientShadow = syncHandler.patch_apply(syncHandler.patch_make(diffs), clientShadow)[0].toString();
		
			clientText = syncHandler.patch_apply(syncHandler.patch_make(diffs), clientText)[0].toString();
		changes++;
	}

	@Override
	public String get() {
		
		return clientText;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public int getChanges() {
		return changes;
	}

	@Override
	public int getsentDataSize() {
		return sentDataSize;
	}
	

}
