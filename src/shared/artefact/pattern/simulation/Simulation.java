package shared.artefact.pattern.simulation;

import java.util.ArrayList;

import org.apache.camel.impl.SimpleRegistry;

import shared.artefact.pattern.ds.DiffClient;
import shared.artefact.pattern.ds.DiffServer;
import shared.artefact.pattern.ot.OTClient;
import shared.artefact.pattern.ot.OTServer;
import shared.artefact.pattern.wiki.WikiClient;
import shared.artefact.pattern.wiki.WikiServer;


public class Simulation {
	
	private final static int DOC_LENGTH = 1000;
	private static ArrayList<ClientThread> otThreads = null;
	private static ArrayList<ClientThread> diffThreads = null;
	private static ArrayList<ClientThread> wikiThreads = null;
	private static Oracle oracle = null;
	private static ArrayList<Config> configs = null;

	public static void main(String[] args) {
		
		int docLength = DOC_LENGTH;

		configs = ConfigLoader.getConfig("config.xml");
		String text = ConfigLoader.getRandomString(docLength, "a".toCharArray());

		
		
		
		for(Config config : configs){
		//############################## Config #############################
			System.out.println("################# Config ##################\nClients: "
					+config.getClients()
					+" Update-Length: "+config.getLength()
					+" Update-Delay: "+config.getTime()+"\n");
			
		//############################## OT #################################Fehler Clients: 10 Update-Length: 20 Update-Delay: 10
			oracle = new Oracle(config.getClients());
			OTServer serverOt = new OTServer("serverOT", text);
			otThreads = setUpThreads(config, oracle, docLength,generateOTClients(config.getClients(), serverOt), SyncTypes.OperationalTransformation);
			oracle.setStartTime();
			startThreads(otThreads);
			
			while(!oracle.getStatus());
			publish(otThreads);

		//############################## Diff ##############################
			
			
			oracle = new Oracle(config.getClients()); 
			SimpleRegistry diffRegistry = new SimpleRegistry();
			DiffServer serverDiff = new DiffServer("serverDiff", diffRegistry, text);
			diffRegistry.put("serverDiff", serverDiff);
			diffThreads = setUpThreads(config, oracle, docLength, generateDiffClients(config.getClients(), diffRegistry), SyncTypes.DifferentialSynchronisation);
			oracle.setStartTime();
			startThreads(diffThreads);
			
			while(!oracle.getStatus());
			publish(diffThreads);
			
		//######################### Wiki ###################
			oracle = new Oracle(config.getClients());
			SimpleRegistry wikiRegistry = new SimpleRegistry();
			WikiServer wikiServer = new WikiServer("serverWiki", wikiRegistry, text);
			wikiRegistry.put("serverWiki", wikiServer);
			wikiThreads = setUpThreads(config, oracle, docLength, generateWikiClients(config.getClients(), wikiRegistry), SyncTypes.Wiki);
			oracle.setStartTime();
			startThreads(wikiThreads);
			
			while(!oracle.getStatus());
			
			wikiServer.stopTimer();
			publish(wikiThreads);
		}
		
	}
	
	private static void publish(ArrayList<ClientThread> threads){
		int changes = 0;
		int sentDataSize = 0;
		SyncTypes type = null;
		long duration = 0;
		
		for(ClientThread thread : threads){
			
			Report report = thread.getReport();
			changes += report.getChanges();
			sentDataSize += report.getSentDataSize();
			type = report.getType();
			duration = report.getDuration();
		}
		
		System.out.println("--------------- "+type+" ---------------\n"+
							"Changes: "+changes+
							"\nSentDataSize: "+sentDataSize+
							" characters \nDuration: "+duration+" ms\n");
	}
	
	private static ArrayList<IClient> generateDiffClients(int amount, SimpleRegistry registry ){
		
		ArrayList<IClient> result = new ArrayList<IClient>();
		
		for(int i = 0; i < amount; i++){
		
			IClient client = new DiffClient("clientDiff"+i, registry, "serverDiff");
			result.add(client);
			registry.put("clientDiff"+i, client);
			
		}
				
		return result;
	}
	
	private static ArrayList<IClient> generateWikiClients(int amount, SimpleRegistry registry) {

		ArrayList<IClient> result = new ArrayList<IClient>();

		for(int i = 0; i < amount; i++){
			WikiClient client = new WikiClient("clientWiki"+i, registry, "serverWiki");
			result.add(client);
			registry.put("clientWiki"+i, client);
		}
		
		return result;
	}
	
	private static ArrayList<IClient> generateOTClients(int amount, OTServer server){
		
		ArrayList<IClient> result = new ArrayList<IClient>();
		SimpleRegistry registry = new SimpleRegistry();
		registry.put(server.getId(), server);
		
		for(int i = 0; i < amount; i++){
			
			result.add(new OTClient("clientOT"+i, registry, server.getId()));
		}
		
		return result;
		
	}
	
	private static ArrayList<ClientThread> setUpThreads(Config config, Oracle oracle, int doclength, ArrayList<IClient> clients, SyncTypes type){
		
		ArrayList<ClientThread> result = new ArrayList<ClientThread>();
		
		for(IClient client : clients){
			
			result.add(new ClientThread(config.getLength(), config.getTime(), oracle, doclength, client, type));
		}
		
		return result;
		
	}
	
	private static void startThreads(ArrayList<ClientThread> threads){
		
		for(ClientThread thread : threads){
			
			new Thread(thread).start();
		}
	}
}