package shared.artefact.pattern.simulation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class ConfigLoader {

	public static ArrayList<Config> getConfig(String filename) {

		ArrayList<Config> result = new ArrayList<Config>();

		File file = new File(filename);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
			
			NodeList nodes = doc.getElementsByTagName("simulation");
			
			for (int i = 0; i < nodes.getLength(); i++) {

				Element element = (Element) nodes.item(i);
				int clients = Integer.parseInt(element.getElementsByTagName("clients").item(0).getTextContent());
				int length = Integer.parseInt(element.getElementsByTagName("length").item(0).getTextContent());
				int time = Integer.parseInt(element.getElementsByTagName("time").item(0).getTextContent());

				result.add(new Config(clients, length, time));
			}

		} catch (ParserConfigurationException e) {

			e.printStackTrace();
		} catch (SAXException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		return result;
	}
	
	public static String getRandomString(int length, char [] allowedCharacters){
		
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
		    char c = allowedCharacters[random.nextInt(allowedCharacters.length)];
		    sb.append(c);
		}
		return sb.toString();
	}

}
