package shared.artefact.pattern.simulation;

import java.util.Random;

public class ClientThread implements Runnable {

	private int length;
	private int delay;
	private Oracle oracle;
	private IClient client;
	private int docLength;
	private SyncTypes type;

	public ClientThread(int length, int time, Oracle oracle, int docLenght, IClient client, SyncTypes type) {

		this.length = length;
		this.delay = time;
		this.oracle = oracle;
		this.client = client;
		this.docLength = docLenght;
		this.type = type;
	}

	@Override
	public void run() {

		oracle.register(client);
		String text = ConfigLoader.getRandomString(length, "01".toCharArray());
		Random random = new Random();
		int pos = random.nextInt(docLength - length - 1);
		client.update(text, pos);

		try {
			Thread.sleep(delay * 1000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		while (!oracle.getStatus()) {

			String checkText = client.get().substring(pos, pos + length);

			if (!checkText.equals(text)) {

				pos = random.nextInt(docLength - length - 1);
				client.update(text, pos);
			}

			oracle.setStatus(client.getId(), client.get());

			try {
				Thread.sleep(delay * 1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

	public Report getReport() {
		return new Report(client.getChanges(), client.getsentDataSize(), oracle.getDuration(), type);
	}
}
