package shared.artefact.pattern.simulation;

public class Config {
	
	private final int clients;
	private final int length;
	private final int time;

	public Config(int clients, int length, int time) {
		this.clients = clients;
		this.length = length;
		this.time = time;
	}
	
	public int getClients() {
		return clients;
	}

	public int getLength() {
		return length;
	}

	public int getTime() {
		return time;
	}

	@Override
	public String toString() {
		return "Config [clients=" + clients + ", length=" + length + ", time="
				+ time + "]";
	}
}
