package shared.artefact.pattern.simulation;

public interface IClient {
	
	/**
	 * The update method updates the current version of the article
	 * 
	 * @param update the article text with the alterations
	 * @return boolean if update was successful or if there is a conflict
	 */
	public void update(String update, int pos);
	
	
	/**
	 * This method will deliver the latest personal version of the object
	 * 
	 * @return String of the current personal version of the object
	 */
	public String get();

	
	/**
	 * This method returns the amount of changes which have been made by the client
	 * @return int amount of changes
	 */
	public int getChanges();
	
	/**
	 * This method returns the amount of sent data.
	 * @return returns the number of sent characters
	 */
	public int getsentDataSize();
	
	/**
	 * This method returns the client's id
	 * @return returns the id of the client
	 */
	public String getId();

}
