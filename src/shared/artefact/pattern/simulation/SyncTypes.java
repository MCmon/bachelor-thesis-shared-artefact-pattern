package shared.artefact.pattern.simulation;

public enum SyncTypes {
	
	OperationalTransformation, Wiki, DifferentialSynchronisation 

}
