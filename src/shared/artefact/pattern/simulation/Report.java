package shared.artefact.pattern.simulation;

public class Report {

	private int changes;
	private int sentDataSize;
	private long duration;
	private SyncTypes type;
	public Report(int changes, int sentDataSize, long time, SyncTypes type) {
		super();
		this.changes = changes;
		this.sentDataSize = sentDataSize;
		this.duration = time;
		this.type = type;
	}

	public int getChanges() {
		return changes;
	}

	public int getSentDataSize() {
		return sentDataSize;
	}

	public long getDuration() {
		return duration;
	}

	public SyncTypes getType() {
		return type;
	}
}
