package shared.artefact.pattern.simulation;

import java.util.Arrays;
import java.util.HashMap;


public class Oracle {
	
	private boolean [] updateFlag;
	private String [] texts;
	private HashMap<String, Integer> collabs;
	private int registryNumber;
	private long start;
	private long end;
	
	
	public Oracle(int amount){
		updateFlag = new boolean[amount];
		Arrays.fill(updateFlag, false);
		texts = new String[amount];
		Arrays.fill(texts, "");
		registryNumber = 0;
		collabs = new HashMap<String, Integer>(); 
	}
	
	public synchronized void register(IClient client){
		
		collabs.put(client.getId(), registryNumber);
		registryNumber++;
	}
	
	public boolean setStatus(String id, String text){
		int i = collabs.get(id);
		updateFlag[i] = true;
		texts[i]= text;
		
		return isAllSync();
	}
	
	public boolean getStatus(){
		return isAllSync(); 
	}
	
	private synchronized boolean isAllSync(){
		
		boolean result = isTextSync();
		
		for(int i = 0; i < updateFlag.length && result; i++){
			
			result = updateFlag[i];
		}
		
		if(result){
			end = System.currentTimeMillis();
		}
		
		return result;
	}
	
	private synchronized boolean isTextSync(){
		
		boolean result = true;
		String text = texts[0];
		
		for(int i = 1; i < texts.length && result; i++){
			
			result = text.equals(texts[i]);

		}
		
		return result;
		
	}
	
	public long getDuration(){
		
		return (end - start);
	}
	
	public void setStartTime(){
		start = System.currentTimeMillis();
	}
}