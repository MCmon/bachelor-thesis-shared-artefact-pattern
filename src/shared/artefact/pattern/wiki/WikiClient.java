package shared.artefact.pattern.wiki;

import org.apache.camel.impl.SimpleRegistry;

import shared.artefact.pattern.simulation.IClient;


public class WikiClient implements IClient {
	
	private String id;
	private SimpleRegistry registry;
	private TransactionObject transObject;
	private WikiServer objectServer;
	private int changes;
	private int sentDataSize;
	
	public WikiClient(String id, SimpleRegistry registry, String objectServerId){
		this.id = id;
		this.registry = registry;
		this.objectServer = registry.lookup(objectServerId, WikiServer.class);
		
		changes = 0;
		sentDataSize = 0;
	}
	
	@Override
	public String get() {
		return objectServer.getLatestVersion(id).getLatestVersion();
	}
	
	@Override
	public void update(String update, int pos) {
		transObject = objectServer.getLatestVersion(id);
		changes++;
		String latestVersion = transObject.getLatestVersion();
		String updateVersion = latestVersion.substring(0, pos)+update+latestVersion.substring(pos+update.length());
		sentDataSize += updateVersion.length();
		transObject.setOwnVersion(updateVersion);
		objectServer.update(transObject);
	}
	
	public void changeNotify(TransactionObject object) {
		if(!object.isSuccess()) {
			sentDataSize += object.getLatestVersion().length()+object.getOwnVersion().length();
		}
	}

	@Override
	public String getId() {
		
		return id;
	}

	@Override
	public int getChanges() {

		return changes;
	}

	@Override
	public int getsentDataSize() {
		
		return sentDataSize;
	}
}
