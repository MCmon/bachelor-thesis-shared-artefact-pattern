package shared.artefact.pattern.wiki;

public class TransactionObject implements Comparable<TransactionObject>{
	
	private String ownVersion;
	private String latestVersion;
	private int transactionId;
	private boolean success;
	private String clientId;
	
	

	public TransactionObject(String ownVersion, String latestVersion, int transactionId, String clientId) {
		
		this.ownVersion = ownVersion;
		this.latestVersion = latestVersion;
		this.transactionId = transactionId;
		this.clientId = clientId;
	}

	public String getOwnVersion() {
		return ownVersion;
	}

	public void setOwnVersion(String ownVersion) {
		this.ownVersion = ownVersion;
	}

	public String getLatestVersion() {
		return latestVersion;
	}

	public void setLatestVersion(String latestVersion) {
		this.latestVersion = latestVersion;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	@Override
	public int compareTo(TransactionObject o) {

	int result;

		if (transactionId < o.transactionId) {
			result = -1;
		} else if (transactionId == o.transactionId) {

			result = 0;
		} else {

			result = 1;
		}

		return result;

	}

}
