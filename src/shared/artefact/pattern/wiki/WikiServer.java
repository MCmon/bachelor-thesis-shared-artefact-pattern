package shared.artefact.pattern.wiki;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.camel.impl.SimpleRegistry;

import com.qarks.util.files.diff.Diff;
import com.qarks.util.files.diff.MergeResult;

public class WikiServer {

	private String id;
	private int transactionId;
	private String text;
	private ConcurrentLinkedQueue<TransactionObject> queue;
	private final SimpleRegistry registry;
	private Timer timer;
	private TimerTask task;
	private int lastWrite;

	public WikiServer(String id, SimpleRegistry registry, String text) {
		this.id = id;
		this.registry = registry;
		this.transactionId = 0;
		this.text = text;
		this.queue = new ConcurrentLinkedQueue<TransactionObject>();
		lastWrite = 0;
		timer = new Timer();
		task = new TimerTask() {

			@Override
			public void run() {

				if (queue != null && queue.size() > 0)

					wikiCollab();
			};

		};

		timer.scheduleAtFixedRate(task, 0, 1);
	}

	/**
	 * This method handles the whole wiki collaboration
	 */
	private void wikiCollab() {

		List<TransactionObject> snapshot = new ArrayList<>();
		synchronized (queue) {

			snapshot.addAll(queue);

			queue = removeAll(queue); // check if empty
		}

		if (snapshot.size() == 1) {

			if (!handleUpdate(snapshot.get(0).getOwnVersion(), snapshot.get(0).getTransactionId())) {

				notifyClient(false, snapshot.get(0).getClientId(), snapshot.get(0));
			}

		} else if (snapshot.size() > 1) {
			// 1. check if first ts is < as lastWrite if so check how to
			// merge
			// (optionally make a method out of the code above)
			// if ts > lastwrite then try to merge until there is a
			// conflict send the version from the first conflict back to
			// the clients
			Collections.sort(snapshot, Collections.reverseOrder());

			TransactionObject ruleWinner = snapshot.get(0);

			if (handleUpdate(ruleWinner.getOwnVersion(), ruleWinner.getTransactionId())) {

				for (int i = 1; i < snapshot.size(); i++) {

					notifyClient(false, snapshot.get(i).getClientId(), snapshot.get(i));
				}
			} else {
				throw new RuntimeException("Excpetion in dealing with three-way merge");
			}

		}
	}

	/**
	 * This method call the notify method in the IWikiClient class. It will
	 * inform if the update and object state
	 * 
	 * @param success
	 *            is true when was successful
	 * @param clientId
	 *            String to lookup the client object in registry
	 * @param response
	 *            is the TransactionObject which contains the information
	 */
	private void notifyClient(boolean success, String clientId, TransactionObject response) {

		WikiClient client = (WikiClient) registry.lookup(clientId);
		response.setSuccess(success);
		response.setLatestVersion(text);
		client.changeNotify(response);
	}

	private synchronized ConcurrentLinkedQueue<TransactionObject> removeAll(
			ConcurrentLinkedQueue<TransactionObject> queue) {

		int length = queue.size();
		for (int i = 0; i < length; i++) {
			queue.remove();
		}
		return queue;
	}

	/**
	 * this method tries to merge the ownVersion string with the originalObject
	 * and updates the lastWrite value if the merge was successful. this method
	 * ! may have sideeffects on lastWrite and originalObject
	 * 
	 * @param ownVersion
	 *            != null
	 * @param transactionId
	 *            >= 0
	 * @return true when there is no conflict otherwhise false is returned
	 */
	private boolean handleUpdate(String ownVersion, int transactionId) {

		boolean successful;

		if (transactionId < lastWrite) {

			MergeResult result = Diff.quickMerge(text, ownVersion, text, true);

			if (result.isConflict()) {
				successful = false;
			} else {
				text = result.getDefaultMergedResult();
				lastWrite = transactionId;
				successful = true;
			}
		} else {
			text = ownVersion;
			lastWrite = transactionId;
			successful = true;
		}

		return successful;
	}

	public void stopTimer() {

		timer.cancel();
		timer.purge();
	}

	public synchronized TransactionObject getLatestVersion(String clientId) {

		TransactionObject result = new TransactionObject(null, text, transactionId, clientId);
		transactionId++;

		return result;
	}

	public void update(TransactionObject object) {
		queue.add(object);
	}
}
