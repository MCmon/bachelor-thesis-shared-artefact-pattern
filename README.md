# Readme #

## What is this repo for? ##

This repository is the pratical part of my bachelor thesis.
It contains three Java implementations of shared artefact patterns.

### How do I get set up? ###

The setup is very easy you just have to clone the repo and run the Simulation class.
For the configuration you have to edit the configuration files and in case you have to change the path to the configuration in the Simultion class.

### Who do I talk to? ###

The repo owner is Simon Krejci ( e1227059@student.tuwien.ac.at )